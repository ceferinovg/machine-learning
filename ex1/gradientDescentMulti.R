#GRADIENTDESCENTMULTI Performs gradient descent to learn theta
#   theta = GRADIENTDESCENTMULTI(x, y, theta, alpha, num_iters) updates theta by
#   taking num_iters gradient steps with learning rate alpha


gradientDescentMulti <- function(X, y, theta, alpha, num_iters) {
	
	m <- length(y)
	
	J.history <- rep(0,num_iters)
	
	for (i in 1:num_iters) {
		
		h <- X %*% theta
		
		error <- h - y
		
		delta <- t(X) %*% error
		
		theta <- theta - (alpha / m) * delta
		
		J.history[i] <- computeCostMulti(X, y, theta)
		
	} 
	
	return(list('theta'= theta, 'J.history' = J.history))
	
}