# NNCOSTFUNCTION Implements the neural network cost function for a two layer
# neural network which performs classification
#   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
#   X, y, lambda) computes the cost and gradient of the neural network. The
#   parameters for the neural network are "unrolled" into the vector
#   nn_params and need to be converted back into the weight matrices. 
# 
#   The returned parameter grad should be a "unrolled" vector of the
#   partial derivatives of the neural network.
#

nnCostFunction <- function (nn_params, input_layer_size, hidden_layer_size,
							num_labels, X, y, lambda) {
								
	Theta1 <- matrix(nn_params[1:c(hidden_layer_size * (input_layer_size + 1))],
					hidden_layer_size, (input_layer_size +1))
	
	Theta2 <- matrix(nn_params[(1 + c(hidden_layer_size * (input_layer_size + 1))):length(nn_params)],
					num_labels, (hidden_layer_size +1))
						
	m <- nrow(X)
	
	J <- 0
	
	Theta1_grad <- matrix(c(0), nrow(Theta1), ncol(Theta1))
	
	Theta2_grad <- matrix(c(0), nrow(Theta2), ncol(Theta2))
		
	I <- diag(num_labels)
	
	Y <- matrix(c(0), m, num_labels)
	
	for (i in 1:m) {
		
	Y[i,] <- I[y[i],]
		
	}
	
	A1 <- cbind(1, X)
	
	Z2 <- A1 %*% t(Theta1)
	
	A2 <- cbind(1, sigmoid(Z2))
	
	Z3 <- A2 %*% t(Theta2)
	
	H <- A3 <- sigmoid(Z3)
	
	penalty <- (lambda / (2*m)) * (sum(sum(Theta1[,2:ncol(Theta1)]^2)) + 
				(sum(sum(Theta2[,2:ncol(Theta2)]^2))))
	
	J <- (1/m) * sum(sum(-Y * log(H) - (1-Y) * log(1-H)))
	
	J <- J + penalty
	
	Sigma3 <- A3 - Y
	
	Sigma2 <- (Sigma3 %*% Theta2 * sigmoidGradient(cbind(1,Z2)))[,-1]
	
	Delta_1 <- t(Sigma2) %*% A1
	
	Delta_2 <- t(Sigma3) %*% A2
	
	Theta1_grad <- Delta_1 / m + (lambda/m)*cbind(0, Theta1[,-1])
	
	Theta2_grad <- Delta_2 / m + (lambda/m)*cbind(0, Theta2[,-1])	
	
	grad <- c(Theta1_grad, Theta2_grad)
	
	return(list('J' = J, 'grad' = grad))
		
}