## Machine Learning Online Class - Exercise 4 Neural Network Learning

# Instructions
# -------------
#
# This file contains code that helps you get started on the
# linear exercise. You will need to complete the following functions
# in this exercise:
#
#	sigmoidGradient.R
#	randInitializeWeights.R
#	nnCostFunction.R
#
# For this exercise, you will not need to change any code in this file,
# or any other files other than those mentioned above.
#

## Initialization

rm(list = ls())

# Setup the parameters you will use for this exercise

input_layer_size <- 400
hidden_layer_size <- 25
num_labels <- 10

## ========== Part 1: Loading and Visualizing Data ==================
# We start the exercise by first loading and visualizing the dataset.
# You will be working with a dataset that contains handwritten digits.
#

# Load training data

cat('Loading and Visualizing Data... \n')

load('ex4data1.RData')

X <- data.matrix(X)

m <- nrow(X)

# Randomly select 100 data points to display

sel <- sample(1:m, 100)

source('displayData.R')

displayData(X[sel,])

readline(prompt= 'Program paused. Press enter to continue.\n')

## ========== Part 2: Loading Parameters ================
#
# In this part of the exercise, we load some pre-initialized
# neural network parameters.

cat('Loading Saved Neural Network Parameters... \n')

# Load the weights into variables Theta1 and Theta2

load('ex4weights.RData')

Theta1 <- data.matrix(Theta1)

Theta2 <- data.matrix(Theta2)

# Unroll parameters

nn_params <- c(Theta1, Theta2)

## ============== Part 3: Compute Cost (Feedforward) =============
# To the neural network, you should first start by implementing the
# feedforward part of the neural network that returns the cost only. You
# should complete the code in nnCostFunction.R to return cost. After
# implementing the feedforward to compute the cost, you can verify that
# your implementation is correct by verifying that you get the same cost
# as us for the fixed debugging parameters.
#
# We suggest implementing the feedforward cost *without* regularization
# first so that it will be easier for you to debug. Later, in part 4, you
# will get to implement the regularized cost.

cat('Feedforward Using Neural Network... \n')

# Weight regularization parameter (we set this to 0 here)

lambda <- 0

source('sigmoid.R')

source('sigmoidGradient.R')

source('nnCostFunction.R')

J <- nnCostFunction(nn_params, input_layer_size, hidden_layer_size,
					num_labels, X, y, lambda)
					
cat('Cost at parameters (loaded from ex4weights): \n', J[[1]])

cat('This value should be about 0.287629')

readline(prompt= 'Program paused. Press enter to continue.\n')

## ============ Part 4: Implement Regularization ==================
# Once your cost function implementation is correct, you should now
# continue to implement the regularization with the cost.
#

cat('Checking Cost Function (w/ Regularization)... \n')

# Weight regularization parameter (we set this to 1 here)

lambda <- 1

J <- nnCostFunction(nn_params, input_layer_size, hidden_layer_size,
					num_labels, X, y, lambda)
				
cat('Cost at parameters (loaded from ex4weights): \n', J[[1]])

cat('This value should be about 0.383770')

readline(prompt= 'Program paused. Press enter to continue.\n')

## ============ Part 5: Sigmoid Gradient ==================
# Before you start implementing the neural network, you will first
# implement the gradient for the sigmoid function. You should complete the
# code in the sigmoidGradient.R file.
#

cat('Evaluating sigmoid gradient...\n')

g <- sigmoidGradient(c(-1, -0.5, 0, 0.5, 1))

cat('Sigmoid gradient evaluated at (-1, -0.5, 0, 0.5, 1): \n', g)

readline(prompt= 'Program paused. Press enter to continue.\n')

## =========== Part 6: Initializing Parameters ================
# In this part of the exercise, you will be starting to implement a two
# layer neural network that classifies digits. You will start by
# implementing a function to initialize the weights of the neural network
# (randInitializeWeights.R)

cat('Initializing Neural Network Parameters... \n')

source('randInitializeWeights.R')

initial_Theta1 <- randInitializeWeights(input_layer_size, hidden_layer_size)
initial_Theta2 <- randInitializeWeights(hidden_layer_size, num_labels)

# Unroll parameters

initial_nn_params <- c(initial_Theta1, initial_Theta2)

## ===========	Part 7: Implement Backpropagation ==============
# Once your cost matches up with ours, you should proceed to implement the
# backpropagation algorithm for the neural network. You should add to the
# code you've written in nnCostFunction.R to return the partial
# derivatives of the parameters.
#

cat('Checking Backpropagation... \n')

# Check gradients by running checkNNGradients

source('debugInitializeWeights.R')
source('computeNumericalGradient.R')
source('checkNNGradients.R')

checkNNGradients()

readline(prompt= 'Program paused. Press enter to continue.\n')

## =========== Part 8: Implement Regularization =============
# Once your backpropagation implementation is correct, you should now
# continue to implement the regularization with the cost and gradient.
#

cat('Checking Backpropagation (w/ Regularization) ... \n')

# Check gradients by running checkNNGradients

lambda <- 3

checkNNGradients(lambda)

# Also output the costFunction debuggin values

debug_J <- nnCostFunction(nn_params, input_layer_size,
					hidden_layer_size, num_labels, X, y, lambda)

cat('Cost at (fixed) debugging parameters (w/ lambda = 3):', debug_J$J)

cat('This value should be about 0.576051')

readline(prompt= 'Program paused. Press enter to continue.\n')


## ======== Part 8: Training NN ==============
# You have now implemented all the code necessary to train a neural
# network. To train your neural network, we will now use 'optim'.

cat('Training Neural Network... \n')

# You should also try different values of lambda

source('nnCost_optim.R')

nn_params <- optim(initial_nn_params, nnCost_optim, input_layer_size = input_layer_size, 
		hidden_layer_size = hidden_layer_size,
				num_labels = num_labels, X = X, y = y, lambda = lambda, 
					method='BFGS', control = list(maxit = 50))
					
# Obtain Theta 1 and Theta2 back from nn_params

Theta1 <- matrix(nn_params$par[1:(hidden_layer_size * (input_layer_size+1))],
					hidden_layer_size, (input_layer_size + 1))
					
Theta2 <- matrix(nn_params$par[(1 + (hidden_layer_size * (input_layer_size +1))):length(nn_params$value)],
					num_labels, (hidden_layer_size + 1))
					
readline(prompt= 'Program paused. Press enter to continue.\n')

# =============== Part 9: Visualize Weights ===================
# You can now 'visualize' what the neural network is learning by
# displaying the hidden units to see what features they are capturing in
# the data.

cat('Visualizing Neural Network... \n')

displayData(Theta1[,2:ncol(Theta1)])

readline(prompt= 'Program paused. Press enter to continue.\n')

# ============= Part 10: Implement Predict =====================

# After training the neural network, we would like to use it to predict
# the labels. You will now implement the 'predict' function to use the
# neural network to predict the labels of the training set. This lets
# you compute the training set accuracy.
#

source('predict.R')

pred <- predict(Theta1, Theta2, X)

cat('Training Set Accuracy: \n', mean(pred==y)) * 100)