## Machine Learning Online Class
# Exercise 8 / Anomaly Detection and Collaborative Filtering
#
# Instructions
# ------------
#
# This file contains code that helps you get started on the
# exercise. You will need to complete the following functions:
#
#	estimateGaussian.R
#	selectThreshold.R
#	cofiCostFunc.R
#
# For this exercise, you will not need to change any code in this
# or any other files other than those mentioned above.
#

# Initialization

rm(list = ls())

## ============= Part 1: Load Example Dataset =================
# We start this exercise by using a small dataset that is easy to
# visualize.
#
# Our example case consists of 2 networks server statistics across
# several machines: the latency and throughput of each machine.
# This exercise will help us find possibly faulty (or very fast)
#

cat ('Visualizing example dataset for outlier detection \n')

# The following command loads the dataset. You should now have the
# variables X, Xval, yval in your environment

load('ex8data1.RData')

# Visualize the example dataset

plot(X[,1], X[,2], pch = 4, col = 'blue', ylab = 'Throughput (mb/s)', 
		xlab = 'Latency (ms)')

readline(prompt= 'Program paused. Press enter to continue.\n')

## ============== Part 2: Estimate the dataset statistics =============
# For this exercise, we assume a Gaussian distribution for the dataset.
#
# We first estimate the parameters of our assumed Gaussian distribution,
# then compute the probabilities for each of the points and then visualize
# both the overall distribution and where each of the points falls in
# terms of that distribution.
#

cat ('Visualizing Gaussian fit. \n')

# Estimate mu and sigma2

source ('estimateGaussian.R')

Gaussian.X <- estimateGaussian(X)

# Returns the density of the multivariate normal at each data point (row)
# of X

source('multivariateGaussian.R')

p <- multivariateGaussian(X, Gaussian.X$mu, Gaussian.X$sigma2)

# Visualize the fit

source('visualizeFit.R')

visualizeFit(X, Gaussian.X$mu, Gaussian.X$sigma2)

readline(prompt= 'Program paused. Press enter to continue.\n')

## =========== Part 3: Find Outliers =============
# Now you will find a good epsilon threshold using a cross-validation set
# probabilities given the estimated Gaussian distribution
#

pval <- multivariateGaussian(Xval, Gaussian.X$mu, Gaussian.X$sigma2)

source('selectThreshold.R')

epsil.F1 <- selectThreshold(yval, pval)

cat('Best epsilon found cross-validation: \n', epsil.F1$bestEpsilon)
cat('this value should be of about 8.99e-05 \n')

cat('Best F1 found on cross-validation set: \n', epsil.F1$bestF1)
cat('this value should be of about 0.875 \n')

# Find the outliers in the training set and plot

outliers <- which(p < epsil.F1$bestEpsilon)

plot(X[,1], X[,2], pch =19)
points(X[outliers, 1], X[outliers, 2], pch =19, col ='red')

readline(prompt= 'Program paused. Press enter to continue.\n')

## =========== Part 4: Multidimensional Outliers =============
# We will now use the code from the previous part and apply it to a
# harder problem in which more features describe each datapoint and only
# some features indicate whether a point is an outlier.
#

# Loads the second dataset. You should now have the
# variables X, Xval, yval in your environment

load('ex8data2.RData')

# Apply the same steps to the larger dataset

Gauss.X <- estimateGaussian(X)

# Training set

p <- multivariateGaussian(X, Gauss.X$mu, Gauss.X$sigma2)

# Cross-validation set

pval <- multivariateGaussian(Xval, Gauss.X$mu, Gauss.X$sigma2)

# Find the best threshold

eps.F1 <- selectThreshold(yval, pval)

cat('Best epsilon found using cross-validation: \n', eps.F1$bestEpsilon)
cat('\n this value should be around 1.38e-18 \n')

cat('Best F1 on Cross Validation Set: \n', eps.F1$bestF1)
cat('\n this value should be around 0.615385 \n')

cat('Outliers found: \n', sum(p < eps.F1$bestEpsilon))