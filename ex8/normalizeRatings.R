# NORMALIZERATINGS Preprocess data by subtracting mean rating for every 
# movie (every row)
#   [Ynorm, Ymean] = NORMALIZERATINGS(Y, R) normalized Y so that each movie
#   has a rating of 0 on average, and returns the mean rating in Ymean.
#


normalizeRatings <- function (Y, R) {
	
	Y <- as.matrix(Y)
	
	Ymean <- rep(0, nrow(Y))
	
	Ynorm <- matrix(c(0), nrow(Y), ncol(Y))
	
	for (i in 1:nrow(Y)) {
		
		idx <- which(R[i,] == 1)
		
		Ymean[i] <- mean(Y[i, idx])
		
		Ynorm[i, idx] <- Y[i, idx] - Ymean[i]
		
	}

	return(list('Ynorm' = Ynorm, 'Ymean' = Ymean))
	
}