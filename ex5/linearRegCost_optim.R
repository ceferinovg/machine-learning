linearRegCost_optim <- function (X, y, theta, lambda) {
	
	m <- length(y)
	
	h <- X %*% theta
	
	errors <- h - y
	
	J <- (sum (errors^2)) / (2 * m)
	
	J + (lambda / (2 * m)) * c(0,theta[2:length(theta)]) %*% c(0,theta[2:length(theta)])	
}
