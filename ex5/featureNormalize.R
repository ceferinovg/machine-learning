# FEATURENORMALIZE Normalizes the features in X 
#   FEATURENORMALIZE(X) returns a normalized version of X where
#   the mean value of each feature is 0 and the standard deviation
#   is 1. This is often a good preprocessing step to do when
#   working with learning algorithms.

featureNormalize <- function(X) {
	
	X <- as.matrix(X)
	
	mu <- colMeans(X)
	
	sigma <- apply(X, 2, sd)
	
	X_norm <- t(apply(X, 1, '-', mu))
	
	X_norm <- t(apply(as.matrix(X_norm), 1, '/', sigma))
	
	return(list('X_norm' = X_norm, 'mu' = mu, 'sigma' = sigma))
	
}