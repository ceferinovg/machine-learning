# PLOTFIT Plots a learned polynomial regression fit over an existing figure.
# Also works with linear regression.
#   PLOTFIT(min_x, max_x, mu, sigma, theta, p) plots the learned polynomial
#   fit with power p and feature normalization (mu, sigma).


plotFit <- function (min_x, max_x, mu, sigma, theta, p) {
	
	x <- seq(min_x - 15, max_x + 25, by = 0.05)
	
	X_poly <- polyFeatures(x, p)
	
	X_poly <- apply(X_poly, 1, '-', mu)
	
	X_poly <- apply(X_poly, 1, '/', sigma)
	
	X_poly <- cbind(1, X_poly)
	
	plot(x, X_poly * theta)
	
}