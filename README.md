#########################################################

MACHINE LEARNING IN R

#########################################################

This is the material of the Coursera MOOC Machine Learning (by Stanford
University), translated to R.

**For each exercise, you just need to enter the folder, open the .pdf file and then
go through the scripts and data within the same folder.**


Few modifications (e. g. the addition of some functions) have been made to the code
in order to guarantee its correct functioning in R.

Please be aware that the purpose of these scripts is educational and
therefore much more efficient R scripts are possible, particularly because I
have completely avoided the loading of packages and libraries. Indeed, functions
estimating a large number of parameters, even if implemented with the
built-in optim() function, are prone to take a significant amount of time.

Get in touch for any comment or suggestion you may have.