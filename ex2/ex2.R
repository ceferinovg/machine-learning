# Machine Learning Online Class - Exercise 2: Logistic Regression
#
# Instructions
# ----------------
#
# This file contains code that helps you get started on the logistic
# regression exercise. You will need to complete the following functions
# in this exercise:
#
#		sigmoid.R
#		costFunction.R
#		predict.R
#		costFunctionReg.R
#
# For this exercise, you will not need to change any code in this file,
# or any other files other than those mentioned above.
#

# Initialization

rm(list = ls())

# Load data
# The first two columns contains the exam scores and the third column
# contains the label

data <- read.table('ex2data1.txt', sep=',')
data <- data.matrix(data)
X <- data[,1:2]
y <- data[,3]

## ========================= Part 1: Plotting ============================

# We start the exercise by first plotting the data to understand the
# problem we are working with.

source('plotData.R')

cat('Plotting data with dots indicating (y = 1) examples and triangles ',
	'indicating (y = 0) examples.\n')
	
plotData(X, y)

title('Exam 1 Score vs Exam 2 Score')

readline(prompt= 'Program paused. Press enter to continue.\n')

## =================== Part 2: Compute Cost and Gradient ==================

# In this part of the exercise, you will implement the cost and gradient
# for logistic regression. You need to complete the code in
# costFunction.R

# Setup the data matrix appropriately, and add ones for the intercept term

# Add intercept term to x and X_test

X <- cbind(1, X)

# Initialize fitting parameters

initial_theta <- rep(0, ncol(X))

# Compute and display initial cost and gradient

source('sigmoid.R')

source('costFunction.R')

cost_grad <- costFunction(initial_theta, X, y)

cat('Cost at initial theta (zeros): \n', cost_grad[[1]])

cat('Expected cost (approx): 0.693 \n')

cat('Gradient at initial theta (zeros): \n', cost_grad[[2]])

cat('Expected gradients (approx): \n -0.1000 -12.0092 -11.2628\n')

# Compute and display cost and gradient with non-zero theta

test_theta <- c(-24, 0.2, 0.2)

cost_grad_nonzero <- costFunction(test_theta, X, y)

cat('Cost at initial theta (zeros): \n', cost_grad_nonzero[[1]])

cat('Expected cost (approx): 0.218 \n')

cat('Gradient at initial theta (zeros): \n', cost_grad_nonzero[[2]])

cat('Expected gradients (approx): \n 0.043 2.566 2.647\n')

readline(prompt= 'Program paused. Press enter to continue.\n')

## =================== Part 3: Optimizing using fminunc ==================

# Have a look at the optim function
# It can do unconstrained minimization using method = 'L-BFGS-B' or Nelder-Mead
# and you can specify an analytical function to compute the gradient
# there's also the package trust

source('cost_optim.R')

theta <- optim(test_theta, cost_optim, X=X, y=y)$par

# Plot Boundary

source('mapFeature.R')

source('plotDecisionBoundary.R')

plotDecisionBoundary(theta, X, y)

title('Exam 1 score vs Exam 2 score')

readline(prompt= 'Program paused. Press enter to continue.\n')

## =================== Part 4: Predict and accuracies ==================

# After learning the parameters, you'll like to use it to predict the outcomes
# on unseen data. In this part, you will use the logistic regression model
# to predict the probability that a student with score 45 on exam 1 and
# score 85 on exam 2 will be admitted.
#
# Furthermore, you will compute the training and test set accuracies of
# our model.
#
# Your task is to complete the code in predict.R

# Predict probability for a student with score 45 on exam 1
# and score 85 on exam 2

prob <- sigmoid(c(1, 45, 85) %*% theta)

cat('For a student with scores 45 and 85, we predict an admission ',
	'probability of \n', prob)

cat('Expected value: 0.775 +/- 0.002\n')

# Compute accuracy on our training set

source('predict.R')

p <- predict(theta, X)

cat('Train accuracy: \n', mean(p==y)*100)
cat('Expected accuracy (approx): 89.0\n')