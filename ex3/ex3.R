## Machine Learning Online Class - Exercise 3 / Part 1: One-vs-all

# Instructions
# --------------
#
# This file contains code that helps you get started on the
# linear exercise. You will need to complete the following functions
# in this exercise:
#
# 		lrCostFunction.R (logistic regression cost function)
#		oneVsAll.R
#		predictOneVsAll.R
#		predict.R
#
#	For this exercise, you will not need to change any code in this file,
#	or any other files other than those mentioned above.
#

# Initialization

rm(list = ls())

# Setup the parameters you will use for this part of the exercise
input_layer_size <- 400

num_labels <- 10

## =============== Part 1: Loading and Visualizing Data ================
# We start the exercise by first loading and visualizing the dataset.
# You will be working with a dataset that contains handwritten digits.
#

# Load Training Data

cat('Loading and Visualizing Data ...\n')

load('ex3data1.RData')

X <- data.matrix(X)

m <- nrow(X)

rand_indices <- sample(1:m, 100, replace=FALSE)

sel <- X[rand_indices,]

source('displayData.R')

displayData(sel)

readline(prompt= 'Program paused. Press enter to continue.\n')

## =============== Part 2a: Vectorize Logistic Regression ===============
# In this part of the exercise, you will reuse your logistic regression
# code from the last exercise. Your task here is to make sure that your
# regularized logistic regression implementation is vectorized. After
# that, you will implement one-vs-all classification for the handwritten
# digit dataset.
#

# Test case for lrCostFunction

source('sigmoid.R')
source('lrCostFunction.R')

theta_t <- c(-2, -1, 1, 2)

X_t <- cbind(1, matrix(c(1:15), 5, 3) / 10)

y_t <- c(1, 0, 1, 0, 1)

lambda_t <- 3

J_grad <- lrCostFunction(theta_t, X_t, y_t, lambda_t)

cat('Cost: \n', J_grad[[1]])
cat('Expected cost: 2.534819 \n')
cat('Gradients: \n', J_grad[[2]])
cat('Expected gradients: \n')
cat('0.146561 -0.548558 0.724722 1.398003 \n')

## ================ Part 2b: One-vs-All Training =====================

cat('Training One-vs-All Logistic Regression...\n')

lambda <- 0.1

source('sigmoid.R')
source('lrCost_optim.R')
source('oneVsAll.R')

all_theta <- oneVsAll(X, y, num_labels, lambda)

readline(prompt= 'Program paused. Press enter to continue.\n')

## =============== Part 3: Predict for One-Vs-All ======================

source('predictOneVsAll.R')

pred <- predictOneVsAll(all_theta, X)

cat('Training Set Accuracy: \n', mean((pred ==y)) * 100)