# PREDICT Predict the label of an input given a trained neural network
#   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
#   trained weights of a neural network (Theta1, Theta2)

predict <- function(Theta1, Theta2, X) {
	
	X <- as.matrix(X)
	
	if (dim(X)[2]==1) {X <- t(X)}
	
	m <- nrow(X)
	
	num_labels <- nrow(Theta2)
	
	p <- rep(0, nrow(X))
	
	X <- cbind(1, X)
	
	for (i in 1:m) {
		
		a1 <- as.matrix(X[i,])
		
		a2 <- c(1, sigmoid(Theta1 %*% a1))
		
		a3 <- sigmoid(Theta2 %*% a2)
		
		p[i] <- max.col(t(a3))
		
	}
	
	return(p)
	
}