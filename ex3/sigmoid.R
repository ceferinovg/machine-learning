# SIGMOID Compute sigmoid function
#   J = SIGMOID(z) computes the sigmoid of z.

sigmoid <- function(z) {
	
	ones <- matrix(c(1), nrow = nrow(as.matrix(z)), ncol = ncol(as.matrix(z)))
	
	g <- ones / (1 + exp(-z))

	return(g)	
}