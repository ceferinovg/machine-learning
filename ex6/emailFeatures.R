emailFeatures <- function (word_indices) {
	
	n <- 1899
	
	x <- rep(0, n)
	
	for (i in 1:length(word_indices)) {
		
		idx <- word_indices[i]
		
		x[idx] <- 1
		
	}
	
	return(x)
	
}