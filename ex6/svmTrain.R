svmTrain <- function (X, Y, C, kernelFunction, tol, max_passes) {
	
	if (missing(tol)) {		
		tol <- 1e-3
	}
	
	if (missing(max_passes)) {
		max_passes <- 5
	}
	
	m <- nrow(X)
	n <- ncol(X)
	
	Y[Y==0] <- -1
	
	alphas <- rep(0, m)
	b <- 0
	E <- rep(0, m)
	passes <- 0
	eta <- 0
	L <- 0
	H <- 0
	
	if (identical(kernelFunction, linearKernel)) {
		K <- X %*% t(X)
	} else if (identical(kernelFunction, gaussianKernel)) {
		X2 <- apply(X^2, 1, sum)
		K <- apply((X2 - 2 * (X %*% t(X))), 1, '+', X2)
		K <- kernelFunction(1, 0, sigma = 0.1) ^ K
	} else {
		
		K <- matrix(c(0), m, m)
		for (i in 1:m) {
			for (j in 1:m) {
				K[i, j] <- kernelFunction(X[i,], X[j,])
				K[j, i] <- K[i, j]
			}
			
		}
	}
	
	cat ('Training...\n')
	
	dots <- 12
	
	while (passes < max_passes) {
		
		num_changed_alphas <- 0
		
		for (i in 1:m) {
			
			E[i] <- b + sum(alphas * Y * K[,i]) - Y[i]
			
			if (((Y[i]*E[i] < -tol) && alphas[i] < C) || ((Y[i]*E[i]) > tol && alphas[i] > 0)) {
				
				j <- ceiling(m*runif(1))
				while (j == i) {
					j <- ceiling(m*runif(1))
				}
							
			}
			
			E[j] <- b + sum(alphas * Y * K[,j]) - Y[j]
				
				alpha_i_old <- alphas[i]
				alpha_j_old <- alphas[j]
				
				if (Y[i] == Y[j]) {
					L <- max(0, alphas[j] + alphas[i] - C)
					H <- min(C, alphas[j] + alphas[i])
				} else {
					L <- max(0, alphas[j] - alphas[i])
					H <- min(C, C + alphas[j] - alphas[i])
				}
				
				if (L == H) {next}
				
				eta <- 2 * K[i, j] - K[i, i] - K[j, j]
				
				if (eta >=0) {next}
				
				alphas[j] <- alphas[j] - (Y[j] * (E[i] - E[j])) / eta
				
				alphas[j] <- min(H, alphas[j])
				alphas[j] <- max(L, alphas[j])
				
				if (abs(alphas[j] - alpha_j_old) < tol) {
					alphas[j] <- alpha_j_old
					next
				}
				
				alphas[i] <- alphas[i] + Y[i]*Y[j]*(alpha_j_old - alphas[j])
				
				b1 <- b -E[i] - Y[i] * (alphas[i] - alpha_i_old) * K[i,j] - Y[j] * (alphas[j] - alpha_j_old) * K[i,j]
				
				b2 <- b -E[j] - Y[i] * (alphas[i] - alpha_i_old) * K[i,j] - Y[j] * (alphas[j] - alpha_j_old) * K[j,j]
				
				if (0 < alphas[i] && alphas[i] < C) {
					b <- b1
				} else if (0 < alphas[j] && alphas[j] < C) {
					b <- b2
				} else {b <- (b1+b2)/2}
				
				num_changed_alphas <- num_changed_alphas +1
			
		}
		
		if (num_changed_alphas == 0) {
			passes <- passes +1
		} else {passes <- 0}
		
		dots <- dots +1
		
		if (dots > 78) {
			dots <- 0
		}
		
		
		
	}
	
	cat('Done! \n')
	
	idx <- which(alphas > 0)
	model <- list()
	
	model$X <- X[idx,]
	model$Y <- Y[idx]
	model$kernelFunction <- kernelFunction
	model$b <- b
	model$alphas <- alphas[idx]
	model$w <- (t(alphas * Y) %*% X)
	
	return(model)
	
}