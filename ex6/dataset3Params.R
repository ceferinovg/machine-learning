dataset3Params <- function (X, y, Xval, yval) {
	
	C <- 1
	
	sigma <- 0.3
	
	values <- c(0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30)
	
	error_min <- Inf
	
	cat ('looking for C and sigma values... \n')
	
	for (C in values) {
		
		for (sigma in values) {
			
			model <- svmTrain(X, y, C, gaussianKernel)
			
			err <- mean(svmPredict(model, Xval))
			
			if (err <= error_min) {
				
				C_final <- C
				
				sigma_final <- sigma
				
				error_min <- err
				
				cat('new min found C, sigma =', sigma_final)
				
			}
			
		}
		
	}
	
	C <- C_final
	
	sigma <- sigma_final
	
	cat('Best value C, sigma =')
	
	return(list('C' = C, 'sigma' = sigma))
	
}