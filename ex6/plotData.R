# PLOTDATA Plots the data points X and y into a new figure 
#   PLOTDATA(x,y) plots the data points with + for the positive examples
#   and o for the negative examples. X is assumed to be a Mx2 matrix.
#
# Note: This was slightly modified such that it expects y = 1 or y = 0


plotData <- function (X, y) {
	
	pos <- which(y == 1)
	
	neg <- which(y == 0)
	
	plot(X[pos, 1], X[pos, 2], pch = 17, col = 'red',
			xlim = c(min(X[,1]), max(X[,1])), ylim = c(min(X[,2]), max(X[,2])))
	
	points(X[neg, 1], X[neg, 2], pch = 19)
	
}