# KMEANSINITCENTROIDS This function initializes K centroids that are to be
# used in K-Means on the dataset X
#   centroids = KMEANSINITCENTROIDS(X, K) returns K initial centroids to be
#   used with the K-Means on the dataset X
#

kMeansInitCentroids <- function (X, K) {
	
	centroids <- matrix(c(0), K, ncol(X))
	
	randidx <- sample(1:nrow(X), nrow(X))
	
	centroids <- X[randidx[1:K],]
	
	return (centroids)
	
}