# FINDCLOSESTCENTROIDS computes the centroid memberships for every example
#   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
#   in idx for a dataset X where each row is a single example. idx = m x 1
#   vector of centroid assignments (i.e. each entry in range [1..K])
#

findClosestCentroids <- function (X, centroids) {
	
	K <- nrow(centroids)
	
	idx <- rep(0, nrow(X))
	
	for (i in 1:nrow(X)) {
		
		distances <- rep(0, nrow(centroids))
		
		for (k in 1:K) {
			
			distances[k] <- sum((X[i,] - centroids[k,])^2)
		}
		
		minIndex <- which(distances==min(distances))[1]
		
		idx[i] <- minIndex
		
	}
	
	return (idx)
	
}