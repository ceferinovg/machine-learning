# RECOVERDATA Recovers an approximation of the original data when using the
# projected data
#   X_rec = RECOVERDATA(Z, U, K) recovers an approximation the
#   original data that has been reduced to K dimensions. It returns the
#   approximate reconstruction in X_rec.
#

recoverData <- function (Z, U, K) {
		
		X_rec <- matrix(c(0), nrow(Z), nrow(U))
		
		Ureduce <- U[,1:K]
		
		for (i in 1:nrow(Z)) {
			
			z <- as.matrix(Z[i,])
			
			x <- Ureduce %*% z
			
			X_rec[i,] <- t(x)
			
		}
	
	return (X_rec)
	
}